﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    Rigidbody rigidBody;
    AudioSource audioSource;

    [SerializeField]
    float thrust = 10f;

    [SerializeField]
    float handling = 30f;

    [SerializeField]
    float rocketVolumeFalloff = 1000f;

    [SerializeField]
    const float maxValue = 1f;


    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            rigidBody.AddRelativeForce(Vector3.up * thrust);
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
            if (audioSource.volume < maxValue)
            {
                audioSource.volume += Time.deltaTime / rocketVolumeFalloff;
            }

        }
        else
        {
            if (audioSource.volume > 0)
            {
                audioSource.volume -= Time.deltaTime / rocketVolumeFalloff;
            }
            else
            {
                audioSource.Stop();
            }

        }
    }


    private void ProcessRotation()
    {
        rigidBody.freezeRotation = true; // take manual control of rotation
        if (Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.forward * Time.deltaTime * handling);
        }
        else if (Input.GetKey(KeyCode.D) && !Input.GetKey(KeyCode.A))
        {
            transform.Rotate(-Vector3.forward * Time.deltaTime * handling);
        }
        rigidBody.freezeRotation = false; // resume physics control
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collided with " + collision.collider.name + " or " + collision.gameObject.name);
        switch (collision.gameObject.tag)
        {
            case "Friendly":
                //do nothing
                break;
            case "Fuel":
                addFuel(collision.gameObject);
                break;
            default:
                Debug.LogError("You have Died!");
                break;
        }
    }

    private void addFuel(GameObject gameObject)
    {
        Debug.Log("Fuel Added");
        gameObject.SetActive(false);
    }
}
